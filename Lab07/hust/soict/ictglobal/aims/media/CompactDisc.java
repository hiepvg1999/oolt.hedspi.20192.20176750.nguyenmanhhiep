package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private int length;
	ArrayList<Track> tracks = new ArrayList<Track>();
	public CompactDisc() {
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public void addTrack(Track track){
		if(tracks.contains(track)){
			System.out.println("The track existed.");
		}else{
			tracks.add(track);
			System.out.println("The track has been added.");
		}
	}
	public void removeTrack(Track track){
		if(tracks.contains(track)){
			tracks.remove(track);
			System.out.println("The track has been removed.");
		}else{
			System.out.println("The track not exist");
		}
	}
	public int getLength(){
		int sum=0;
		for(Track tr: tracks){
			sum+= tr.getLength();
		}
		return sum;
	}
	public void play(){
    	System.out.println("Artist: "+this.getArtist());
    	System.out.println("CompactDisc length: "+this.getLength());
    	for(Track tr: tracks){
    		tr.play();
    	}
    }

}
