package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.MemoryDaemon;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
	public static ArrayList<Order> ordersList = new ArrayList<Order>();
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	public static DigitalVideoDisc InputDVD(){
		DigitalVideoDisc dvd = new DigitalVideoDisc();
		Scanner sc = new Scanner(System.in);
		System.out.print("Length: ");
		dvd.setLength(Integer.parseInt(sc.nextLine()));
		System.out.print("Cost: ");
		dvd.setCost(Float.parseFloat(sc.nextLine()));
		System.out.print("Title: ");
		dvd.setTitle(sc.nextLine());
		System.out.print("Category: ");
		dvd.setCategory(sc.nextLine());
		System.out.print("Director: ");
		dvd.setDirector(sc.nextLine());
		return dvd;
	}
	public static CompactDisc InputCD(){
		CompactDisc cd = new CompactDisc();
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		cd.setTitle(sc.nextLine());
		System.out.print("Category: ");
		cd.setCategory(sc.nextLine());
		System.out.print("Director: ");
		cd.setDirector(sc.nextLine());
		System.out.print("Artist: ");
		cd.setArtist(sc.nextLine());
		System.out.print("Cost: ");
		cd.setCost(Float.parseFloat(sc.nextLine()));
		int flag=1;
		int i=1;
		do{
			if(flag==1){
				Track tr = new Track();
				System.out.print("Title of track "+i+": ");
				tr.setTitle(sc.nextLine());
				System.out.print("Length of track "+i+": ");
				tr.setLength(Integer.parseInt(sc.nextLine()));
				cd.addTrack(tr);
				i++;
			}
			
			System.out.print("Enter flag= ");
			flag= Integer.parseInt(sc.nextLine());
		}while(flag==1);
		return cd;
	}
	public static Book InputBook(){
		Book book= new Book();
		Scanner sc = new Scanner(System.in);
		System.out.print("Title: ");
		book.setTitle(sc.nextLine());
		System.out.print("Category: ");
		book.setCategory(sc.nextLine());
		System.out.print("Cost: ");
		book.setCost(Float.parseFloat(sc.nextLine()));
		int flag=1;
		int i=1;
		do{
			if(flag==1){
				System.out.print("Author "+i+": ");
				String author= sc.nextLine();
				book.addAuthor(author);
				i++;
			}		
			System.out.print("Enter flag= ");
			flag= Integer.parseInt(sc.nextLine());
		}while(flag==1);
		return book;
	}
	public static void addItemToOrder(DigitalVideoDisc dvd){
		ordersList.get(ordersList.size()-1).addMedia(dvd);
	}
	public static void addItemToOrder(CompactDisc cd){
		ordersList.get(ordersList.size()-1).addMedia(cd);
	}
	public static void addItemToOrder(Book book){
		ordersList.get(ordersList.size()-1).addMedia(book);
	}
    public static void main(String[] args) {
    MemoryDaemon md = new MemoryDaemon();
    Thread thread = new Thread(md);
    thread.setDaemon(true);
    int flag;
    
    Scanner sc = new Scanner(System.in);
    do{
    	showMenu();
    	System.out.print("Enter flag= ");
    	flag = Integer.parseInt(sc.nextLine());
    	switch(flag){
    	case 1:{
    	final Order order = new Order();
    	ordersList.add(order);
    	System.out.println("The order has been created");
    	break;
    	}
    	case 2:{
    	int code;
    	System.out.print("Nhap code: ");
    	code = Integer.parseInt(sc.nextLine());
    	if(code==1){
    		DigitalVideoDisc dvd;
    		dvd=InputDVD(); 
    		addItemToOrder(dvd);
    		int dd;
    		do{
    			System.out.print("Do you want to play it?");
    			dd = Integer.parseInt(sc.nextLine());
    			if(dd==1){
    			//Yes
    				dvd.play();
    			}
    		}while(dd==1);
    	}else if(code==2){
    		CompactDisc cd;
    		cd = InputCD();
    		addItemToOrder(cd);
    		int dd;
    		do{
    			System.out.print("Do you want to play it?");
    			dd = Integer.parseInt(sc.nextLine());
    			if(dd==1){
    			//Yes
    				cd.play();
    			}
    		}while(dd==1);
    	}else if(code ==3){
    		Book book;
    		book = InputBook();
    		addItemToOrder(book);
    	}
    	break;
    	}
    	case 3:{
    		int index;
    		System.out.print("Enter delete index: ");
    		index = sc.nextInt();
    		ordersList.get(ordersList.size()-1).removeMedia(index);
    		break;
    	}
    	case 4:{
    		ordersList.get(ordersList.size()-1).listOfItem();
    		break;
    	}
    	case 0:{
    		System.out.println("Thanks");
    		break;
    	}
    	default:{
    		System.out.println("Error");
    		break;
    	}
    	}
    	 
    }while(flag!=0);
    sc.close();
    md.run();
    }

}
