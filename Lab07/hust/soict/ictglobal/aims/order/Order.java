package hust.soict.ictglobal.aims.order;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
public class Order {
	public static final int MAX_NUMBERS_ORDERED =10;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private Date dateOrdered;
	public static final int MAX_LIMITTED_ORDERS=5;
	private static int nbOrders=0;
	public Order(){
		super();
		this.dateOrdered = new Date();
		if(nbOrders<MAX_LIMITTED_ORDERS){
			nbOrders++;
			System.out.println("The order "+nbOrders+" has been add");
		}else{
			System.out.println("The order hasnt been add");
			System.exit(-1);
		}
	}
	
	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}

	public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void addMedia(Media m){
		itemsOrdered.add(m);
		System.out.println("Media has been added!");
	}
	public void removeMedia(Media m){
		if(itemsOrdered.contains(m)){
			itemsOrdered.remove(m);
			System.out.println("Remove successfully!");
		}else{
			System.out.println("Media not exist!");
		}
	}
	public void removeMedia(int index){
		if(index>=itemsOrdered.size()){
			System.out.println("Error");
		}else{
			itemsOrdered.remove(index);
		}
	}
	public void listOfItem(){
		for(Media m: itemsOrdered){
			System.out.println(m.getTitle());
			System.out.println(m.getCategory());
			System.out.println(m.getCost());
			System.out.println("-------------------------");
		}
	}
	public float totalCost(){
		float sum=0;
		for (Media media : itemsOrdered){
			sum+= media.getCost();
		}
		return sum;
	}
}
