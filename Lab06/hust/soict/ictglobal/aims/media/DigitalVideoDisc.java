package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media{
	private String director;
	private int length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc(){
		
	}
	public DigitalVideoDisc(String title){
		super(title);
	}
	public DigitalVideoDisc(String category,String title){
		super(category,title);
	}
    public DigitalVideoDisc(String director,String title, String category){
		super(title,category);
		this.director= director;
	}
    public DigitalVideoDisc(String title, String category,String director,int length,float cost){
    	super(title,category,cost);
    	this.director =director;
    	this.length =length;
    }
    public boolean search(String title){
    	title= title.trim();
    	String[] token = title.split(" ");
    	boolean flag=true;
    	for(int i=0;i<token.length;i++){
    		if(title.contains(token[i])==false){
    			return false;
    		}
    	}
    	return true;
    }
}
