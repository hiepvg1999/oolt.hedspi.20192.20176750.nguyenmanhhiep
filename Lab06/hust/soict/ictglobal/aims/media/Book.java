package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class Book extends Media{
    private ArrayList<String> authors  = new ArrayList<String>();
   
	public Book() {
	}
    public Book(String title,String category,float cost){
    	super(title,category,cost);
    }
	public ArrayList<String> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName){
		if(authors.contains(authorName)){
			System.out.println("Author existed");
		}else{
			authors.add(authorName);
			System.out.println("Add successfully");
		}
	}
	public void removeAuthor(String authorName){
		if(authors.contains(authorName)){
			authors.remove(authorName);
			System.out.println("Remove successfully");
		}else{
			System.out.println("Author not exist");
		}
	}

}
