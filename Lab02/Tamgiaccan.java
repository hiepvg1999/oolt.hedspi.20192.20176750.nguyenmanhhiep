package Lab02;

import java.util.Scanner;

public class Tamgiaccan {
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap n= ");
		n = sc.nextInt();
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n-i;j++){
				System.out.print(" ");
			}
			for(int j=n-i+1;j<=n+i-1;j++){
				System.out.print("*");
			}
			System.out.print("\n");
		}
		sc.close();
		System.exit(0);
	}
}
