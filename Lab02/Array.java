package Lab02;

import java.util.ArrayList;
import java.util.Collections;

public class Array {
	public static void main(String[] args) {
		int sum=0;
		ArrayList<Integer> array = new ArrayList<Integer>();
		array.add(5);
		array.add(10);
		array.add(3);
		array.add(2);
		array.add(11);
		for(Integer i: array){
			sum+=i;
		}
		float avg = sum/(float)array.size();
		Collections.sort(array);
		System.out.println("Sum : "+sum+" Avg: "+avg);
		for(Integer i: array){
			System.out.print(i+" ");
		}
	}
}
