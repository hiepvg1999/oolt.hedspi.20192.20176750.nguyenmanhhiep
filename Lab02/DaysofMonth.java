package Lab02;

import java.util.Scanner;

public class DaysofMonth {
	public static boolean checkYear(int year){
		if(year%400==0) return true;
		if(year%4==0 && year %100!=0) return true;
		return false;
	}
	public static void main(String[] args) {
		int year,month;
		Scanner sc = new Scanner(System.in);
		do{
			String input;
			System.out.print("Year/Month: ");
			input = sc.nextLine();
			String[] output= input.split("/");
			year = Integer.parseInt(output[0]);
			month = Integer.parseInt(output[1]);
		}while(year <0 || month<1 || month>12);
		System.out.print("Number days of month "+month+" year "+year+" : ");
		switch(month){
		case 1: case 3: case 5: case 7: case 8: case 10: case 12:{
			System.out.println("31");
			break;
		}
		case 4: case 6: case 9: case 11:{
			System.out.println("30");
		}
		case 2:{
			if(checkYear(year)){
				System.out.println("29");
			}else{
				System.out.println("28");
			}
		}
		
		}
		sc.close();
	}
}
