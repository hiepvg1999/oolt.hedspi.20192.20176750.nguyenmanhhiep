package Lab03;

public class Order {
	public static final int MAX_NUMBERS_ORDERED =10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered=0;
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered<MAX_NUMBERS_ORDERED){
			itemsOrdered[qtyOrdered]=disc;
			qtyOrdered++;
			System.out.println("This disc has been add!");
		}else{
			System.out.println("The order is almost full!");
		}
	}
	public float totalCost(){
		float sum=0;
		for(int i=0;i<qtyOrdered;i++){
			sum+=itemsOrdered[i].getCost();
		}
		return sum;
	}
	public void listOfItem(){
		for(int i=0;i<qtyOrdered;i++){
			System.out.println(itemsOrdered[i].getTitle());
		}
	}
	public void remoteItem(DigitalVideoDisc disc){
		boolean flag =false;
			for(int i=0;i<qtyOrdered;i++){
				if(disc == itemsOrdered[i]){
					for(int j=i+1;j<qtyOrdered;j++){
						itemsOrdered[i]=itemsOrdered[j];
					}
					flag =true;
				}
				if(flag){
					qtyOrdered--;
					break;
				}
			}
		
		
		
	}
}
