package Lab03;

public class Aims {
public static void main(String[] args) {
	Order anOrder = new Order();
	DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
	dvd1.setCategory("Animation");
	dvd1.setDirector("Roger Allers");
	dvd1.setCost(20.0f);
	dvd1.setLength(87);
	anOrder.addDigitalVideoDisc(dvd1);
	DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star War");
	dvd2.setCategory("Science Fition");
	dvd2.setDirector("George");
	dvd2.setCost(30.0f);
	dvd2.setLength(100);
	anOrder.addDigitalVideoDisc(dvd2);
	DigitalVideoDisc dvd3 = new DigitalVideoDisc("Test");
	dvd3.setCategory("Animation");
	dvd3.setDirector("Roger Allers1");
	dvd3.setCost(100.0f);
	dvd3.setLength(89);
	anOrder.addDigitalVideoDisc(dvd3);
	DigitalVideoDisc dvd4 = new DigitalVideoDisc("One piece");
	dvd4.setCategory("Animation");
	dvd4.setDirector("Odda");
	dvd4.setCost(150.0f);
	dvd4.setLength(150);
	anOrder.addDigitalVideoDisc(dvd4);
	System.out.print("Total is: ");
	System.out.println(anOrder.totalCost());
	anOrder.listOfItem();
	anOrder.remoteItem(dvd2);
	System.out.println();
	anOrder.listOfItem();
}
}
