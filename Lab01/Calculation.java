package Lab01;
import javax.swing.JOptionPane;
public class Calculation {
	public static void main(String[] args) {
		String strNum1,strNum2;
		double num1,num2;
		String S_Mess = "Sum of two numbers ";
		String D_Mess = "Different of two numbers ";
		String P_Mess = "Product of two numbers ";
		String Q_Mess = "Quotient of two numbers ";
		strNum1 =JOptionPane.showInputDialog(null, "Please input the first number: ","Input the first number",JOptionPane.INFORMATION_MESSAGE);
		strNum2 =JOptionPane.showInputDialog(null, "Please input the second number: ","Input the second number",JOptionPane.INFORMATION_MESSAGE);
		num1 = Double.parseDouble(strNum1);
		num2 = Double.parseDouble(strNum2);
		S_Mess += (strNum1+" and "+strNum2);
		D_Mess += (strNum1+" and "+strNum2);
		P_Mess += (strNum1+" and "+strNum2);
		Q_Mess += (strNum1+" and "+strNum2);
		String err= "Can't divide into zero";
		JOptionPane.showMessageDialog(null, S_Mess+" is: "+(num1+num2),"Sum",JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, D_Mess+" is: "+(num1-num2),"Different",JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, P_Mess+" is: "+(num1*num2),"Product",JOptionPane.INFORMATION_MESSAGE);
		if(num2 !=0){
			JOptionPane.showMessageDialog(null, Q_Mess+" is: "+(num1/num2),"Quotient",JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, err);
		}
		System.exit(0);
	}
}
