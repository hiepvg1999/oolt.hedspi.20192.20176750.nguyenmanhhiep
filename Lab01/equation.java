package Lab01;
import java.util.Scanner;

import javax.swing.JOptionPane;
public class equation {
	public static void PTbac1_1an(int a,int b){
		String note;
		if(a==0){
			if(b==0){
				note="PT vo so nghiem";
				JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
			}else{
				note="PT vo nghiem";
				JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
			}
		}else{
			note="PT co nghiem duy nhat la: "+(b/(float)a);
			JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
		}
	}
	public static void PTbac1_2an(int a1,int b1,int c1,int a2,int b2,int c2){
		int D,Dx,Dy;
		String note;
		D=a1*b2-a2*b1;
		Dx=c1*b2-c2*b1;
		Dy=a1*c2-a2*c1;
		if(D!=0){
			note="Phuong trinh co nghiem duy nhat la: x= "+(Dx/(float)D)+" va y= "+(Dy/(float)D);
			JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
		}else{
			if(Dx==0&& Dy==0){
				note="Phuong trinh vo so nghiem";
				JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
			} else{
				note="PT vo nghiem";
				JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	public static void PTbac2_1an(int a,int b,int c){
		String note;
		float delta = b*b-4*a*c;
		if(delta<0){
			note="PT vo nghiem";
			JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
		}else if(delta==0){
			note="PT co nghiem kep la:x= "+(-b/(2*(float)a));
			JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
		}else{
			note="PT co 2 nghiem phan biet la x1= "+((-b-Math.sqrt(delta))/(2*(float)a))+" va x2= "+((-b+Math.sqrt(delta))/(2*(float)a));
			JOptionPane.showMessageDialog(null, note,"Status",JOptionPane.INFORMATION_MESSAGE);
		}
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int flag;
		do{
			System.out.print("Nhap lua chon: ");
			flag= sc.nextInt();
			switch (flag) {
			case 0:{
				int a,b;
				System.out.println("Nhap a= ");
			    a=sc.nextInt();
			    System.out.println("Nhap b= ");
			    b=sc.nextInt();
				PTbac1_1an(a,b);
				break;
			}
			case 1:{
				int a1,b1,c1,a2,b2,c2;
				System.out.println("Nhap a1,b1,c1= ");
			    a1=sc.nextInt();
			    b1=sc.nextInt();
			    c1=sc.nextInt();
			    System.out.println("Nhap a2,b2,c2= ");
			    a2=sc.nextInt();
			    b2=sc.nextInt();
			    c2=sc.nextInt();
				PTbac1_2an(a1,b1,c1,a2,b2,c2);
				break;
			}
			case 2:{
				int a,b,c;
				System.out.println("Nhap a= ");
			    a=sc.nextInt();
			    System.out.println("Nhap b= ");
			    b=sc.nextInt();
			    System.out.println("Nhap c= ");
			    c=sc.nextInt();
				PTbac2_1an(a,b,c);
				break;
			}
			case 3:{
				System.out.println("Xin cam on!");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Nhap tu 0 den 2!");
				break;
			}
		}while(flag!=3);
		sc.close();
	}
}
